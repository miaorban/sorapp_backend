var dateFormat = require('dateformat');

module.exports = {
  validateBreweryToUpsert: (req,res,next) => {
    var err = []
    console.log(req.body);
    if (req.body.name === '') err.push({error: 'Név megadása kötelező'})
    if (req.body.city === '') err.push({error: 'Város megadása kötelező'})
    if (req.body.country === '') err.push({error: 'Ország megadása kötelező'})
    if (err.length == 0) next()
    else res.send({error: err})
  }

}
