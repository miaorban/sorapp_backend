var dateFormat = require('dateformat');

module.exports = {
  validateBeerUpsert: (req,res,next) => {
    var err = []
    if (req.body.brewery === '') err.push({error: 'Sörfőzde megadása kötelező.'})
    if (req.body.name === '') err.push({error: 'Név megadása kötelező'})
    if (isNaN(req.body.ibu)) err.push({error: 'Az ibu-nak számnak kell lennie'})
    if (isNaN(req.body.abv)) err.push({error: 'Az alkoholtartalomnak számnak kell lennie'})
    if (isNaN(req.body.srm)) err.push({error: 'Az srm-nek számnak kell lennie'})
    if (isNaN(req.body.upc)) err.push({error: 'Az upc-nek számnak kell lennie'})
    if (err.length == 0) next()
    else res.send({error: err})
  }

}
