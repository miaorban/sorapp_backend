var dateFormat = require('dateformat');

module.exports = {
  prepareBreweryToUpsert: (req,res,next) => {
    var brewery = req.body

    brewery.updated = dateFormat( Date.now(), "yyyy-mm-dd hh:MM:ss");

    brewery.type = 'brewery'

    console.log()
    var documentId = brewery.name.toLowerCase().replace(" ", "_")

    req.body = {brewery: JSON.stringify(brewery), documentId: documentId}
    next()
  }

}
