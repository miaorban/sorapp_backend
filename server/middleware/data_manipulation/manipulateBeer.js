var dateFormat = require('dateformat');

module.exports = {
  prepareBeerToUpsert: (req,res,next) => {
    var beer = req.body

    beer.brewery.id ? beer.brewery_id = beer.brewery.id :  beer.brewery_id = beer.brewery_id
    delete beer.brewery

    beer.updated = Date.now()
    beer.updated = dateFormat(beer.updated, "yyyy-mm-dd hh:MM:ss");

    beer.type = 'beer'

    // nem tudtam eldonteni, hogy lehetne egyszeruen id-t adni a dokumentumnak, igy csak generalok egy szamot neki
    var documentId
    beer.id ? documentId = beer.id : documentId = '_' + Math.random().toString(36).substr(2, 9)
    delete beer.id

    req.body = {beer: JSON.stringify(beer), documentId: documentId}
    next()
  }

}
