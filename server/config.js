const couchbaseConf = {
  endPoint    : 'couchbase://localhost/travel-sample',
  bucket      : 'beer-sample',
  bucketname  : '`beer-sample`',
  user        : 'Administrator',
  password    : 'password',
}


var couchbase = require('couchbase');
var cluster = new couchbase.Cluster(couchbaseConf.endPoint);
cluster.authenticate(couchbaseConf.user, couchbaseConf.password)
const bucket = cluster.openBucket(couchbaseConf.bucket);


module.exports = {
  // Couchbase  : couchbaseConf
  CouchbaseBucket : bucket,
  bucketname      : couchbaseConf.bucketname
}
