const config = require ('../config')
const express = require ('express')
const router = express.Router()
var couchbase = require('couchbase')

// middleware importok ellenorzeshez, adatok modositasahoz
var {validateBeerUpsert} = require('../middleware/validation/validateBeer')
var {prepareBeerToUpsert} = require('../middleware/data_manipulation/manipulateBeer')


/**
 * Legmagasabb alkoholtartalmú sörök lekérése.
 * @route GET beer/abv
 * @group Sörök - Sörökkel kapcsolatos operációk
 * @param {Number} limit.param.required - lekért sörfőzdék száma
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.get('/abv/:limit', function(req, res){
  query = `SELECT name, abv FROM ${config.bucketname} order by abv desc limit ${req.params.limit}`
  var q = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(q, [], (error, beers) => {
        if (error) {
          res.status(500).send(error)
        } else {
          res.send(beers)
        }
      });

})


/**
 * Sörfök adatainak lekérése.
 * @route GET beer/collect
 * @group Sörök - Sörökkel kapcsolatos operációk
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.get('/collect', function(req, res){
  query = ` SELECT beer.name, beer.style, beer.category, beer.abv, beer.ibu, beer.srm, beer.upc, beer.brewery_id, brewery.name AS brewery, META(beer).id FROM ${config.bucketname} beer
            JOIN ${config.bucketname} brewery ON beer.brewery_id = META(brewery).id
            WHERE beer.type="beer"
            ORDER BY beer.name asc`
  var q = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(q, [], (error, beers) => {
        if (error) {
          res.status(500).send(error)
        } else {
          res.send(beers)
        }
      })
})

/**
 * Sör adatainak lekérése név alapján.
 * @route GET beer/getbyname
 * @group Sörök - Sörökkel kapcsolatos operációk
 * @param {String} name.body.required - lekért sör neve
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.get('/getbyname/:name/:breweryid', function(req, res){
  query =   ` SELECT beer.*, brewery.name AS breweryname, brewery.city AS brewerycity, brewery.country AS brewerycountry FROM ${config.bucketname} beer
              JOIN ${config.bucketname} brewery ON beer.brewery_id = META(brewery).id
              WHERE beer.type = 'beer' AND beer.name = '${req.params.name}' AND beer.brewery_id = '${req.params.breweryid}'`
  var q = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(q, [], (error, beers) => {
        if (error) {
          res.status(500).send(error)
        } else {
          res.send(beers)
        }
      });
})

/**
 * Sör lehetséges kategóriáinak lekérése.
 * @route GET beer/collectcategory
 * @group Sörök - Sörökkel kapcsolatos operációk
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.get('/collect/categories', function(req, res){
  query = ` SELECT DISTINCT category FROM ${config.bucketname} WHERE category IS NOT NULL`
  var N1Query = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(N1Query, [], (error, categories) => {
        if (error) {
          res.status(500).send(error)
        } else {
          res.send(categories)
        }
      });
})

/**
 * Új sör mentése vagy meglévő módosítása.
 * @route POST beer/upsert
 * @group Sörök - Sörökkel kapcsolatos operációk
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.post('/upsert', validateBeerUpsert, prepareBeerToUpsert, function(req, res){
  query = ` UPSERT INTO ${config.bucketname} ( KEY, VALUE ) VALUES ( "${req.body.documentId}", ${req.body.beer}) RETURNING META().id as docid, * `
  var N1Query = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(N1Query, [], (error, beer) => {
        if (error) {
          res.status(500).send(error)
        } else {
          console.log('beer: ', beer);
          res.send({msg: 'ok'})
        }
      });
})

/**
 * Új sör mentése vagy meglévő módosítása.
 * @route POST beer/upsert
 * @group Sörök - Sörökkel kapcsolatos operációk
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.post('/delete', function(req, res){
  query = `DELETE FROM ${config.bucketname} WHERE META().id="${req.body.beer_id}" RETURNING name`
  var N1Query = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(N1Query, [], (error, beer) => {
        if (error) {
          res.status(500).send(error)
        } else {
          if (!beer[0]) res.send('sikertelen torles')
          else res.status(200).send('OK')
        }
      });
})

module.exports = router
