const config = require ('../config')
const express = require ('express')
const router = express.Router()
var couchbase = require('couchbase');

var {prepareBreweryToUpsert} = require('../middleware/data_manipulation/manipulateBrewery')
var {validateBreweryToUpsert} = require('../middleware/validation/validateBrewery')

/**
 * Utoljára frissített sörfőzdék neveinek lekérése.
 * @route GET brewery/lastUpdated
 * @group Sörfőzdék - Sörfőzdékkel kapcsolatos operációk
 * @param {Number} limit.body.required - lekért sörfőzdék száma
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.get('/lastUpdated/:limit', function(req, res){
  query = `SELECT name, updated FROM ${config.bucketname} WHERE type="brewery" ORDER BY updated DESC LIMIT ${req.params.limit}`
  var q = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(q, [], (error, breweries) => {
        if (error) {
          res.status(500).send(error)
        } else {
          res.send(breweries)
        }
      });

})


/**
 * Sörfőzdék nevének, helyének lekérése.
 * @route GET brewery/collect
 * @group Sörfőzdék - Sörfőzdékkel kapcsolatos operációk
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.get('/collect', function(req, res){
  query = `SELECT name, city, country, address FROM ${config.bucketname} WHERE type="brewery" ORDER BY name ASC`
  var q = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(q, [], (error, breweries) => {
        if (error) {
          res.status(500).send(error)
        } else {
          res.send(breweries)
        }
      });
})

/**
 * Sörfőzdék nevének lekérése.
 * @route GET brewery/collectnames
 * @group Sörfőzdék - Sörfőzdékkel kapcsolatos operációk
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.get('/collectnames', function(req, res){
  query = `SELECT name,  meta(${config.bucketname}).id AS id FROM ${config.bucketname} WHERE type="brewery" ORDER BY name asc`
  var q = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(q, [], (error, breweries) => {
        if (error) {
          res.status(500).send(error)
        } else {
          res.send(breweries)
        }
      });
})


/**
 * Sörfőzde adatainak, söreinek lekérése név alapján.
 * @route GET brewery/getbyname
 * @group Sörök - Sörökkel kapcsolatos operációk
 * @param {String} name.body.required - lekért sörfőzde neve
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.get('/getbyname/:name', function(req, res){
  // ez mukodik, csak nesttel kellene, h ne ismetlodjon a sorfozde adata minden sorre
  // query =  `SELECT brewery.*, beer.name AS beername FROM ${config.bucketname} beer
  //           JOIN ${config.bucketname} brewery ON META(brewery).id = beer.brewery_id
  //           WHERE brewery.type = 'brewery' AND brewery.name = '21st Amendment Brewery Cafe'`
  // KERDES: miert nem jo a feladatban megadott pelda? (kulonbseg: META(brewery).id brewery.id helyett)
  query =  `SELECT *
            FROM ${config.bucketname} brewery
            NEST ${config.bucketname} beer ON
            beer.type = 'beer'
            AND beer.brewery_id = META(brewery).id
            WHERE brewery.name = '${req.params.name}'`

  var q = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(q, [], (error, breweries) => {
        if (error) {
          res.status(500).send(error)
        } else {
          res.send(breweries)
        }
      });
})

/**
 * Új sörfőzde mentése vagy meglévő módosítása.
 * @route POST brewery/upsert
 * @group Sörfőzdék - Sörfőzdékkel kapcsolatos operációk
 * @returns {object} 'ok' -
 * @returns {Error}  500 - Hiba oka.
 */
router.post('/upsert', validateBreweryToUpsert, prepareBreweryToUpsert, function(req, res){
  query = ` UPSERT INTO ${config.bucketname} ( KEY, VALUE ) VALUES ( "${req.body.documentId}", ${req.body.brewery}) RETURNING META().id as docid, * `
  var q = couchbase.N1qlQuery.fromString(query);
      config.CouchbaseBucket.query(q, [], (error, beer) => {
        if (error) {
          res.status(500).send(error)
        } else {
          res.status(200).send({msg:'ok'})
        }
      });
})

module.exports = router
