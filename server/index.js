
const express = require('express')
const consola = require('consola')
const path = require('path')
const { Nuxt, Builder } = require('nuxt')
const app = express()
const host = process.env.HOST || 'localhost'
const port = process.env.PORT || 3002
const bodyParser = require('body-parser')

const expressSwagger = require('express-swagger-generator')(app);

let options = {
    swaggerDefinition: {
        info: {
            description: 'Korszerű adatbázisok nagy beadandó api.',
            title: 'Folyamatszabályozás api',
            version: '1.0.0',
        },
        host: 'localhost:3002',
        basePath: '',
        produces: [
            "application/json",
            "application/xml"
        ],
        schemes: ['http', 'https'],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'x-access-token',
                description: "",
            }
        }
    },
    basedir: __dirname, //app absolute path
    files: ['../server/**/*.js'] //Path to the API handle folder
};
expressSwagger(options)


app.set('port', port)

// ez a ketto kell ahhoz, hogy a req.body ne legyen undefined
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = (process.env.NODE_ENV === 'development')

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
var brewery = require ('./routes/brewery.js')
var beer = require ('./routes/beer.js')

// Register custom routes
app.use ('/brewery', brewery)
app.use ('/beer', beer)

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()

// Custom routes

module.exports = app
